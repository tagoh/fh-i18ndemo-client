var app = angular.module('helloI18N', []);
app.controller('helloController', function($scope) {
    $scope.languages = [
	{id: 0, name: 'en'},
	{id: 1, name: 'ja'},
    ];
    $scope.lang = $scope.languages[0];
    $scope.send = function() {
	document.getElementById('cloudResponse').innerHTML = "<p>Calling Cloud.....</p>";
	$fh.cloud(
	    {
		path: 'hello',
		data: {
		    hello: document.getElementById('hello_to').value,
		    lang: $scope.lang.name
		}
	    },
	    function (res) {
		document.getElementById('cloudResponse').innerHTML = "<p>" + res.msg + "</p>";
	    },
	    function (code, errorprops, params) {
		alert('An error occured: ' + code + ' : ' + errorprops);
	    }
	);
    };
});
